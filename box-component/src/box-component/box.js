import React, { Component } from 'react'
import './box.css'

class Box extends Component {
        constructor(props) {
                super(props);
                this.state = {
                  bgColor: "red"
                }
        }
            
            
        boxClick = (e) => {
                this.setState({
                  bgColor: "gray"
                })
        }
            
        render() {
                return (
                        <div className="App">
                
                                <h1 style={{textAlign:'center'}}>Box Component</h1>  
                                
                                <div className="boxClick" 
                                style={{backgroundColor: this.state.bgColor}}
                                onClick={this.boxClick}>
                                        <h1> <span>Click Me!</span></h1>
                                </div>
                        
                        </div> 
                );
        }
}


export default Box

